#!/bin/sh
touch pid.file
kill `cat pid.file`
echo "starting server"
nohup java -jar ./build/libs/veeretusesimulaator-0.0.1-SNAPSHOT.jar > log.txt 2> errors.txt < /dev/null &
echo "server started (i think)"
echo $! > pid.file
