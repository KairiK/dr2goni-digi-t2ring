package ee.valiit.veeretusesimulaator;

import java.util.ArrayList;

public class Tegevused {

    //D6 veeretuse simulaator
    //kui veeretab 1 või 6, peab veeretust kordama
    //kõik veeretused lähevad ArrayListi
    //viimane number ArrayListis on tulemus, millele saab liita atribuudid
    public static ArrayList<Integer> veeretused () {
        ArrayList<Integer> veeretused = new ArrayList();
        int veeretus = (int) (Math.random() * 6 + 1);
        veeretused.add(veeretus);
        while (veeretus == 6 || veeretus == 1) {
            veeretus = (int) (Math.random() * 6 + 1);
            veeretused.add(veeretus);
        }
        int tulemus = 0;

        for (int member : veeretused) {
            if (member == 1){
                tulemus -= 6;
            } else {
                tulemus += member;
            }
        }
        veeretused.add(tulemus);

        return veeretused;
    }

    /*
    kasKaklus boolean - kui on lähirünnak, siis on true, kui on kaugrünnak, siis on false
     */
    public static ArrayList<Integer> pihtasaamine(ArrayList<Integer> katse, Tegelane tegelane, boolean kasKaklus) {
        //  ArrayList veeretused = veeretused();
        int tulemus = katse.get(katse.size()-1);

        if (kasKaklus) {
            tulemus += tegelane.getKaklus() - tegelane.getVigastus();
        } else {
            tulemus += tegelane.getLaskmine() - tegelane.getVigastus();
        }

        katse.set(katse.size()-1 ,tulemus);

        return katse;
    }

    public static ArrayList<Integer> kahjustus(ArrayList<Integer> katse, Tegelane tegelane, boolean kasKaklus) {
        //  ArrayList veeretused = veeretused();
        int tulemus = katse.get(katse.size()-1);

        if (kasKaklus) {
            tulemus += tegelane.getKaklus() - tegelane.getVigastus();
        } else {
            tulemus += tegelane.getLaskmine() - tegelane.getVigastus();
        }

        katse.set(katse.size()-1 ,tulemus);

        return katse;
    }

    public static ArrayList<Integer> paasemine(ArrayList<Integer> katse, Tegelane tegelane) {
        //  ArrayList veeretused = veeretused();
        int tulemus = katse.get(katse.size() - 1);
        tulemus += tegelane.getRefleks() - tegelane.getVigastus();
        katse.set(katse.size() - 1 , tulemus);
        return katse;
    }

    public static ArrayList elujoud(Tegelane tegelane) {
        ArrayList<Integer> veeretus1 = veeretused();
        int tulemus1 = veeretus1.get(veeretus1.size()-1);
        System.out.println("Tulemus1: " + veeretus1);

        ArrayList<Integer> veeretus2 = veeretused();
        int tulemus2 = veeretus2.get(veeretus2.size()-1);
        System.out.println("Tulemus2: " + veeretus2);

        if (tulemus1 > tulemus2) {
            tulemus1 += 2 * tegelane.getSitkus() - tegelane.getVigastus();
            veeretus1.set(veeretus1.size()-1 ,tulemus1);
            tulemus2 += tegelane.getSitkus() - tegelane.getVigastus();
            veeretus2.set(veeretus2.size()-1 ,tulemus2);
        } else if (tulemus1 < tulemus2){
            tulemus1 += tegelane.getSitkus() - tegelane.getVigastus();
            veeretus1.set(veeretus1.size()-1 ,tulemus1);
            tulemus2 += 2 * tegelane.getSitkus() - tegelane.getVigastus();
            veeretus2.set(veeretus2.size()-1 ,tulemus2);
        } else {
            tulemus1 += 2 * tegelane.getSitkus() - tegelane.getVigastus();
            veeretus1.set(veeretus1.size()-1 ,tulemus1);
            tulemus2 += tegelane.getSitkus() - tegelane.getVigastus();
            veeretus2.set(veeretus2.size()-1 ,tulemus2);
        }

        ArrayList veeretusteTulemused = new ArrayList();
        veeretusteTulemused.add(veeretus1);
        veeretusteTulemused.add(veeretus2);
        System.out.println("Veeretus1: " + veeretus1);
        System.out.println("Veeretus2: " + veeretus2);

        return veeretusteTulemused;
    }
}
