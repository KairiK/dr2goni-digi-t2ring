package ee.valiit.veeretusesimulaator;

public class VooruAlustamine {
    private String rynnakVsKaitse;
    private String lahiVsKaug;
    private int vigastus;
    private String tegelane;

    public VooruAlustamine() {

    }

    public String getRynnakVsKaitse() {
        return rynnakVsKaitse;
    }

    public void setRynnakVsKaitse(String rynnakVsKaitse) {
        this.rynnakVsKaitse = rynnakVsKaitse;
    }

    public String getLahiVsKaug() {
        return lahiVsKaug;
    }

    public void setLahiVsKaug(String lahiVsKaug) {
        this.lahiVsKaug = lahiVsKaug;
    }

    public int getVigastus() {
        return vigastus;
    }

    public void setVigastus(int vigastus) {
        this.vigastus = vigastus;
    }

    public String getTegelane() {
        return tegelane;
    }

    public void setTegelane(String tegelane) {
        this.tegelane = tegelane;
    }
}
