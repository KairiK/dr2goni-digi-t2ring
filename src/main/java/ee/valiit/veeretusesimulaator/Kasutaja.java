package ee.valiit.veeretusesimulaator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RestController;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
@RestController

public class Kasutaja {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private String kasutaja;
    private String parool;

    public Kasutaja(){

    }

    public Kasutaja(String kasutaja, String parool){
        this.kasutaja = kasutaja;
        this.parool = parool;
    }

    public String getKasutaja() {
        return kasutaja;
    }

    public String getParool() {
        return parool;
    }


    /*kommenteerin välja, aga jätan kommentaarina alles, ehk läheb pärast tarvis

    public boolean kontrolliKontot(Kasutaja kasutaja) {
        System.out.println(jdbcTemplate);
        String sqlKask = "SELECT * FROM kasutajakontod;";
        ArrayList l = (ArrayList) jdbcTemplate.queryForList(sqlKask);

       // String vastus = jdbcTemplate.queryForObject(sqlKask, String.class);
        System.out.println(l);
        return true;

        String sqlKask = "SELECT EXISTS (SELECT * FROM kasutajakontod WHERE kasutaja = '" + kasutaja.getKasutaja() + "' AND parool = '" + kasutaja.getParool() + "');";

        boolean vastus = jdbcTemplate.queryForObject(sqlKask, Boolean.class);
        System.out.println(vastus);
        return vastus;

    }
    */
}
