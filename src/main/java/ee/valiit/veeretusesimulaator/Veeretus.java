package ee.valiit.veeretusesimulaator;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Veeretus {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public Date aeg;
    public String tegelane;
    public String veeretused;
    public String lahing;

    public Veeretus(Timestamp timestamp) {
        System.out.println("--------------------");
        if (timestamp != null) {
           this.aeg = new Date(timestamp.getTime());
        }
    }
}
