package ee.valiit.veeretusesimulaator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
public class APIController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public APIController() {
    }


    @PostMapping("/index")
    String sisseLogimine(@RequestBody Kasutaja kasutaja) {
        boolean kasutajaOige = false;
        boolean molemadOiged = false;
        String sqlKaskKasutaja = "SELECT EXISTS (SELECT * FROM kasutajakontod WHERE kasutaja = '" + kasutaja.getKasutaja() + "');";
        kasutajaOige = jdbcTemplate.queryForObject(sqlKaskKasutaja, Boolean.class);
        System.out.println(kasutajaOige);

        String sqlKaskParool = "SELECT EXISTS (SELECT * FROM kasutajakontod WHERE kasutaja = '" + kasutaja.getKasutaja() + "' AND parool = '" + kasutaja.getParool() + "');";
        molemadOiged = jdbcTemplate.queryForObject(sqlKaskParool, Boolean.class);
        System.out.println(molemadOiged);
        String vastus = "";
        if (!kasutajaOige) {
            vastus = "Vale kasutajanimi";
        } else {
            if (!molemadOiged) {
                vastus = "Vale parool";
            } else {
                vastus = "Kõik OK";
            }
        }
        System.out.println(vastus);
        return vastus;

    }

    @PostMapping("/tegelase_loomine")
    public ArrayList<Tegelane> tagastaOlemasolevTegelane(@RequestBody HashMap<String, String> kasutaja) {
        System.out.println("tagastaOlemasolevTegelane käivitus!");
        System.out.println(kasutaja);
        String kasutajaNimi = kasutaja.get("kasutaja");
        System.out.println(kasutajaNimi);
        ArrayList<Tegelane> yksTegelane = new ArrayList<>();
        yksTegelane = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM tegelased WHERE kasutaja='" + kasutajaNimi + "';");
        System.out.println(yksTegelane);
        return yksTegelane;
    }

    @PostMapping("/tegelase_kinnitus")
    public void kinnitaOlemasolevTegelane(@RequestBody Tegelane tegelane) {
        System.out.println("kinnitaOlemasOlevTegelane käivitus!");
        System.out.println(tegelane);
        String sqlKask = "UPDATE tegelased SET kaklus = " + tegelane.getKaklus() + ", laskmine = " + tegelane.getLaskmine() + ", refleks = " + tegelane.getRefleks() + ", sitkus = " + tegelane.getSitkus() + ", vaist = " + tegelane.getVaist() + ", volu = " + tegelane.getVolu() + " WHERE tegelane = '" + tegelane.getNimi() + "';";
        System.out.println(sqlKask);
        jdbcTemplate.execute(sqlKask);
        System.out.println("Tegelane uuendatud");
    }

    @PostMapping("/mis_vigastused")
    //
    public ArrayList<Tegelane> kasOnVigastus(@RequestBody HashMap<String, String> tegelane) {
        System.out.println("kasOnVigastus käivitus!");
        System.out.println(tegelane);
        String tegelaseNimi = tegelane.get("tegelane");
        System.out.println(tegelaseNimi);
        ArrayList<Tegelane> yksTegelane = new ArrayList<>();
        yksTegelane = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM tegelased WHERE tegelane='" + tegelaseNimi + "';");
        System.out.println(yksTegelane);
        return yksTegelane;
    }

    // Vooru alustamise jaoks vajalike atribuutide saamine ja klassi loomine
    @PostMapping("/vooru_alustamine")
    public String vooruAlustamine(@RequestBody VooruAlustamine vooruAlustamine) {
        System.out.println("vooruAlustamine: " + vooruAlustamine.getTegelane());
        String sqlKask = "UPDATE tegelased SET vigastus = " + vooruAlustamine.getVigastus() + " WHERE tegelane = '" + vooruAlustamine.getTegelane() + "';";
        jdbcTemplate.execute(sqlKask);
        return "";
    }

    @PostMapping("/pihtasaamine")
    public ArrayList<Integer> pihtasaamine(@RequestBody Pihtasaamine pihtasaamine) {
        System.out.println("Pihtasaamine: " + pihtasaamine.getTegelane());
        ArrayList<Integer> veeretused = Tegevused.veeretused();
        System.out.println("Veeretused: " + veeretused);
        String lahiVsKaug = pihtasaamine.getLahiVsKaug();
        boolean kasKaklus = false;
        if (lahiVsKaug.equals("lahi")) {
            kasKaklus = true;
        }
        ArrayList<Tegelane> yksTegelane = (ArrayList) jdbcTemplate.query("SELECT * FROM tegelased WHERE tegelane='" + pihtasaamine.getTegelane() + "';", (resultSet, rowNum) -> {
            String kasutaja = resultSet.getString("kasutaja");
            String nimi = resultSet.getString("tegelane");
            int kaklus = resultSet.getInt("kaklus");
            int laskmine = resultSet.getInt("laskmine");
            int refleks = resultSet.getInt("refleks");
            int sitkus = resultSet.getInt("sitkus");
            int vaist = resultSet.getInt("vaist");
            int volu = resultSet.getInt("volu");
            int vigastus = resultSet.getInt("vigastus");
            boolean aktiivne = true;

            return new Tegelane(kasutaja, nimi, kaklus, laskmine, refleks, sitkus, vaist, volu, vigastus, aktiivne);
        });
        System.out.println(yksTegelane.get(0).getKaklus());

        Tegelane tegelane = new Tegelane(
                yksTegelane.get(0).getKasutaja(),
                yksTegelane.get(0).getNimi(),
                yksTegelane.get(0).getKaklus(),
                yksTegelane.get(0).getLaskmine(),
                yksTegelane.get(0).getRefleks(),
                yksTegelane.get(0).getSitkus(),
                yksTegelane.get(0).getVaist(),
                yksTegelane.get(0).getVolu(),
                yksTegelane.get(0).getVigastus(),
                yksTegelane.get(0).isAktiivne());


        ArrayList<Integer> veeretuseTulemus = Tegevused.pihtasaamine(veeretused, tegelane, kasKaklus);
        System.out.println("veeretuseTulemus: " + veeretuseTulemus);
        jdbcTemplate.execute("INSERT INTO veeretused VALUES(" +
                "NOW(), '" +
                pihtasaamine.getTegelane() + "', '" +
                veeretuseTulemus.toString() + "', " +
                "'pihtasaamine, " + pihtasaamine.getLahiVsKaug() + "');");
        return veeretuseTulemus;
    }

    @PostMapping("/paasemine")
    public ArrayList<Integer> paasemine(@RequestBody HashMap<String, String> sisendTegelane) {
        String tegelaseNimi = sisendTegelane.get("tegelane");

        ArrayList<Integer> veeretused = Tegevused.veeretused();
        ArrayList<Tegelane> yksTegelane = (ArrayList) jdbcTemplate.query("SELECT * FROM tegelased WHERE tegelane='" + tegelaseNimi + "';", (resultSet, rowNum) -> {
            String kasutaja = resultSet.getString("kasutaja");
            String nimi = resultSet.getString("tegelane");
            int kaklus = resultSet.getInt("kaklus");
            int laskmine = resultSet.getInt("laskmine");
            int refleks = resultSet.getInt("refleks");
            int sitkus = resultSet.getInt("sitkus");
            int vaist = resultSet.getInt("vaist");
            int volu = resultSet.getInt("volu");
            int vigastus = resultSet.getInt("vigastus");
            boolean aktiivne = true;

            return new Tegelane(kasutaja, nimi, kaklus, laskmine, refleks, sitkus, vaist, volu, vigastus, aktiivne);
        });
        Tegelane tegelane = new Tegelane(
                yksTegelane.get(0).getKasutaja(),
                yksTegelane.get(0).getNimi(),
                yksTegelane.get(0).getKaklus(),
                yksTegelane.get(0).getLaskmine(),
                yksTegelane.get(0).getRefleks(),
                yksTegelane.get(0).getSitkus(),
                yksTegelane.get(0).getVaist(),
                yksTegelane.get(0).getVolu(),
                yksTegelane.get(0).getVigastus(),
                yksTegelane.get(0).isAktiivne());


        ArrayList<Integer> veeretuseTulemus = Tegevused.paasemine(veeretused, tegelane);
        jdbcTemplate.execute("INSERT INTO veeretused VALUES(" +
                "NOW(), '" +
                tegelane.getNimi() + "', '" +
                veeretuseTulemus.toString() + "', " +
                "'pääsemine');");
        return veeretuseTulemus;
    }

    @PostMapping("/elujoud")
    public ArrayList elujoud(@RequestBody HashMap<String, String> sisendTegelane) {
        String tegelaseNimi = sisendTegelane.get("tegelane");
        ArrayList<Integer> veeretused = Tegevused.veeretused();

        ArrayList<Tegelane> yksTegelane = (ArrayList) jdbcTemplate.query("SELECT * FROM tegelased WHERE tegelane='" + tegelaseNimi + "';", (resultSet, rowNum) -> {
            String kasutaja = resultSet.getString("kasutaja");
            String nimi = resultSet.getString("tegelane");
            int kaklus = resultSet.getInt("kaklus");
            int laskmine = resultSet.getInt("laskmine");
            int refleks = resultSet.getInt("refleks");
            int sitkus = resultSet.getInt("sitkus");
            int vaist = resultSet.getInt("vaist");
            int volu = resultSet.getInt("volu");
            int vigastus = resultSet.getInt("vigastus");
            boolean aktiivne = true;

            return new Tegelane(kasutaja, nimi, kaklus, laskmine, refleks, sitkus, vaist, volu, vigastus, aktiivne);
        });
        Tegelane tegelane = new Tegelane(
                yksTegelane.get(0).getKasutaja(),
                yksTegelane.get(0).getNimi(),
                yksTegelane.get(0).getKaklus(),
                yksTegelane.get(0).getLaskmine(),
                yksTegelane.get(0).getRefleks(),
                yksTegelane.get(0).getSitkus(),
                yksTegelane.get(0).getVaist(),
                yksTegelane.get(0).getVolu(),
                yksTegelane.get(0).getVigastus(),
                yksTegelane.get(0).isAktiivne());


        ArrayList veeretuseTulemus = Tegevused.elujoud(tegelane);
        System.out.println("Veeretusetulemus: " + veeretuseTulemus);
        jdbcTemplate.execute("INSERT INTO veeretused VALUES (NOW(), '" +
                tegelaseNimi + "', '" +
                veeretuseTulemus.toString() + "', " +
                "'elujõud');");

        return veeretuseTulemus;

    }

    @RequestMapping("/uuesti")
    public ArrayList<Integer> uuestiVeeretus() {
        ArrayList<Integer> veeretused = Tegevused.veeretused();
        jdbcTemplate.execute("INSERT INTO veeretused VALUES(NOW(), 'kordusveeretus', '" + veeretused.toString() + "', 'kordus');");

        return veeretused;
    }


    @RequestMapping("/uuestiElujoud")
    public ArrayList uuestiElujoud() {
        ArrayList<Integer> v1 = Tegevused.veeretused();
        ArrayList<Integer> v2 = Tegevused.veeretused();
        ArrayList veeretusedKokku = new ArrayList();
        veeretusedKokku.add(v1);
        veeretusedKokku.add(v2);
        jdbcTemplate.execute("INSERT INTO veeretused VALUES(NOW(), 'kordusveeretused', '" + veeretusedKokku.toString() + "', 'kordus: elujõud');");
        return veeretusedKokku;

    }

    @GetMapping("/kysikasutajad")
    ArrayList<String> kysiKasutajad() {
        try {
            String sqlKask = "SELECT kasutaja FROM kasutajakontod;";
            ArrayList<String> kasutajad = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {
                String kasutaja = resultSet.getString("kasutaja");
                return new String(kasutaja);
            });
            System.out.println(kasutajad);
            return kasutajad;
        } catch (DataAccessException err) {
            System.out.println("Table was not ready");
            return new ArrayList();
        }
    }

    @GetMapping("/kysitegelased")
    ArrayList<String> kysiTegelased() {
        try {
            String sqlKask = "SELECT tegelane FROM tegelased;";
            ArrayList<String> tegelased = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {
                String tegelane = resultSet.getString("tegelane");
                return new String(tegelane);
            });
            System.out.println(tegelased);
            return tegelased;
        } catch (DataAccessException err) {
            System.out.println("Table was not ready");
            return new ArrayList();
        }
    }

    @PostMapping("/kasutaja_muutmine")
    String kasutaja_muutmine(@RequestBody Kasutaja kasutaja) {
        String sqlKask = "UPDATE kasutajakontod SET parool = '" + kasutaja.getParool() + "' WHERE kasutaja = '" + kasutaja.getKasutaja() + "';";
        jdbcTemplate.execute(sqlKask);
        return "";

    }

    @PostMapping("/kasutaja_kustutamine")
    String kasutaja_kustutamine(@RequestBody HashMap<String, String> kasutajaHashMap) {
        String kasutaja = kasutajaHashMap.get("kasutaja");
        System.out.println(kasutajaHashMap);
        String sqlKaskKasutaja = "DELETE FROM kasutajakontod WHERE kasutaja = '" + kasutaja + "';";
        jdbcTemplate.execute(sqlKaskKasutaja);
        String sqlKaskTegelane = "DELETE FROM tegelased WHERE kasutaja = '" + kasutaja + "';";
        jdbcTemplate.execute(sqlKaskTegelane);
        return "";

    }

    @PostMapping("/tegelase_muutmine")
    String tegelase_muutmine(@RequestBody Tegelane tegelane) {
        String sqlKask = "UPDATE tegelased SET kaklus = " + tegelane.getKaklus() + ", laskmine = " + tegelane.getLaskmine() + ", refleks = " + tegelane.getRefleks() + ", sitkus = " + tegelane.getSitkus() + ", vaist = " + tegelane.getVaist() + ", volu = " + tegelane.getVolu() + ", vigastus = " + tegelane.getVigastus() + ", aktiivne = " + tegelane.isAktiivne() + " WHERE tegelane = '" + tegelane.getNimi() + "';";
        jdbcTemplate.execute(sqlKask);
        return "";

    }

    @PostMapping("/tegelase_kustutamine")
    String tegelase_kustutamine(@RequestBody HashMap<String, String> tegelaseHashMap) {
        String tegelane = tegelaseHashMap.get("tegelane");
        System.out.println(tegelaseHashMap);
        String sqlKask = "DELETE FROM tegelased WHERE tegelane = '" + tegelane + "';";
        jdbcTemplate.execute(sqlKask);
        return "";

    }

    @PostMapping("/tegelase_seis")
    public ArrayList<Tegelane> tagastaTegelane(@RequestBody HashMap<String, String> tegelane) {
        System.out.println("tagastaOlemasolevTegelane käivitus!");
        System.out.println(tegelane);
        String tegelaseNimi = tegelane.get("tegelane");
        System.out.println(tegelaseNimi);
        ArrayList<Tegelane> yksTegelane = new ArrayList<>();
        yksTegelane = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM tegelased WHERE tegelane='" + tegelaseNimi + "';");
        System.out.println(yksTegelane);
        return yksTegelane;
    }

    @PostMapping("/loo_uus_kasutaja")
    String looUusKasutaja(@RequestBody Kasutaja kasutaja) {
        boolean isKasutaja = false;
        String sqlKaskKasutaja = "SELECT EXISTS (SELECT * FROM kasutajakontod WHERE kasutaja = '" + kasutaja.getKasutaja() + "');";
        String sqlKaskLooUusKasutaja = "INSERT INTO kasutajakontod VALUES ('" + kasutaja.getKasutaja() + "', '" + kasutaja.getParool() + "');";
        isKasutaja = jdbcTemplate.queryForObject(sqlKaskKasutaja, Boolean.class);
        System.out.println(isKasutaja);
        String vastus = "";
        if (isKasutaja) {
            vastus = "Kasutaja juba olemas.";
        } else {
            jdbcTemplate.execute(sqlKaskLooUusKasutaja);
            vastus = "Uus kasutaja loodud.";
        }

        System.out.println(vastus);
        return vastus;
   }

    @PostMapping("/loo_uus_tegelane")
    String looUusTegelane(@RequestBody Tegelane tegelane) {
        boolean isTegelane = false;
        String sqlKaskTegelane = "SELECT EXISTS (SELECT * FROM tegelased WHERE tegelane = '" + tegelane.getNimi() + "');";
        String sqlKaskLooUusTegelane = "INSERT INTO tegelased VALUES ('"
                                                                      + tegelane.getKasutaja() + "', '"
                                                                      + tegelane.getNimi() + "', "
                                                                      + tegelane.getKaklus() + ", "
                                                                      + tegelane.getLaskmine() + ", "
                                                                      + tegelane.getRefleks() + ", "
                                                                      + tegelane.getSitkus() + ", "
                                                                      + tegelane.getVaist() + ", "
                                                                      + tegelane.getVolu() + ", "
                                                                      + tegelane.getVigastus() + ", "
                                                                      + tegelane.isAktiivne() + ");";
        isTegelane = jdbcTemplate.queryForObject(sqlKaskTegelane, Boolean.class);
        String vastus = "";
        if (isTegelane) {
            vastus = "Tegelane juba olemas.";
        } else {
            jdbcTemplate.execute(sqlKaskLooUusTegelane);
            vastus = "Uus tegelane loodud.";
        }

        System.out.println(vastus);
        return vastus;
    }

    @GetMapping("/veeretusteajalugu")
    ArrayList veeretusteajalugu(){
        String sqlKask = "SELECT * FROM veeretused;";
        ArrayList veeretused = (ArrayList) jdbcTemplate.queryForList(sqlKask);

        return veeretused;
    }

    @PostMapping("/kustuta")
    String kustuta(){
        jdbcTemplate.execute("TRUNCATE veeretused;");
        return "";
    }
}
