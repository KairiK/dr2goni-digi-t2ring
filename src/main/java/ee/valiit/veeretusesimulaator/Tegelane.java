package ee.valiit.veeretusesimulaator;

public class Tegelane {
    private String kasutaja;
    private String nimi;
    private int kaklus;
    private int laskmine;
    private int refleks;
    private int sitkus;
    private int vaist;
    private int volu;
    private int vigastus;
    private boolean aktiivne;

    public Tegelane(String kasutaja, String nimi, int kaklus, int laskmine, int refleks, int sitkus, int vaist, int volu, int vigastus, boolean aktiivne) {
        this.kasutaja = kasutaja;
        this.nimi = nimi;
        this.kaklus = kaklus;
        this.laskmine = laskmine;
        this.refleks = refleks;
        this.sitkus = sitkus;
        this.vaist = vaist;
        this.volu = volu;
        this.vigastus = vigastus;
        this.aktiivne = aktiivne;
    }

    public  Tegelane() {

    }

    public String getNimi() {
        return nimi;
    }

    public int getKaklus() {
        return kaklus;
    }

    public void setKaklus(int kaklus) {
        this.kaklus = kaklus;
    }

    public int getLaskmine() {
        return laskmine;
    }

    public void setLaskmine(int laskmine) {
        this.laskmine = laskmine;
    }

    public int getRefleks() {
        return refleks;
    }

    public void setRefleks(int refleks) {
        this.refleks = refleks;
    }

    public int getSitkus() {
        return sitkus;
    }

    public void setSitkus(int sitkus) {
        this.sitkus = sitkus;
    }

    public int getVaist() {
        return vaist;
    }

    public void setVaist(int vaist) {
        this.vaist = vaist;
    }

    public int getVolu() {
        return volu;
    }

    public void setVolu(int volu) {
        this.volu = volu;
    }

    public int getVigastus() {
        return vigastus;
    }

    public void setVigastus(int vigastus) {
        this.vigastus = vigastus;
    }

    public boolean isAktiivne() {
        return aktiivne;
    }

    public String getKasutaja() {
        return kasutaja;
    }

    public void setKasutaja(String kasutaja) {
        this.kasutaja = kasutaja;
    }
}
