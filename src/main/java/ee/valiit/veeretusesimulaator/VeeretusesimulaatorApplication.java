package ee.valiit.veeretusesimulaator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class VeeretusesimulaatorApplication implements CommandLineRunner {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(VeeretusesimulaatorApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		System.out.println("Configure database tables");
		jdbcTemplate.execute("DROP TABLE IF EXISTS kasutajakontod;");
		jdbcTemplate.execute("DROP TABLE IF EXISTS tegelased;");
		jdbcTemplate.execute("DROP TABLE IF EXISTS veeretused;");

		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS kasutajakontod (kasutaja TEXT, parool TEXT);");
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS tegelased (" +
				"kasutaja TEXT, " +
				"tegelane TEXT, " +
				"kaklus  SMALLINT, " +
				"laskmine SMALLINT, " +
				"refleks SMALLINT, " +
				"sitkus SMALLINT, " +
				"vaist SMALLINT, " +
				"volu SMALLINT, " +
				"vigastus SMALLINT, " +
				"aktiivne BOOLEAN);");
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS veeretused(" +
				"aeg TIMESTAMPTZ, " +
				"tegelane TEXT, " +
				"veeretused TEXT, " +
				"lahing TEXT);");

		jdbcTemplate.execute("INSERT INTO kasutajakontod VALUES ('admin', 'parool');");

		//järgnev osa kunagi ära kustutada!!! :)
		jdbcTemplate.execute("INSERT INTO veeretused VALUES (NOW(), 'Saara', '[1, 6, 11]', 'pääsemine, kaug');");
		jdbcTemplate.execute("INSERT INTO veeretused VALUES (NOW(), 'Peedu', '[2, 7, 12]', 'pihtasaamine, kaug');");
		jdbcTemplate.execute("INSERT INTO kasutajakontod VALUES ('aiki', 'parool11');");
		jdbcTemplate.execute("INSERT INTO tegelased VALUES ('aiki', 'kadu', 1, 2, 3, 0, 1, 2, 1, TRUE);");
		jdbcTemplate.execute("INSERT INTO kasutajakontod VALUES ('Kairi', 'parool11');");
		jdbcTemplate.execute("INSERT INTO tegelased VALUES ('Kairi', 'Saara', 2, 1, 1, 2, 0, 1, 0, TRUE);");
		jdbcTemplate.execute("INSERT INTO kasutajakontod VALUES ('Jann', 'parool11');");
		jdbcTemplate.execute("INSERT INTO tegelased VALUES ('Jann', 'troll', 3, 0, 1, 2, 4, 2, 3, TRUE);");
		jdbcTemplate.execute("INSERT INTO kasutajakontod VALUES ('Laura', 'parool11');");
		jdbcTemplate.execute("INSERT INTO tegelased VALUES ('Laura', 'Lilleke', 3, 5, 2, 4, 5, 2, 0, TRUE);");
		jdbcTemplate.execute("INSERT INTO kasutajakontod VALUES ('Pärtel', 'parool11');");
		jdbcTemplate.execute("INSERT INTO tegelased VALUES ('Pärtel', 'mõmmi', 1, 2, 3, 2, 1, 2, 1, TRUE);");
	}
}
