package ee.valiit.veeretusesimulaator;

public class Pihtasaamine {

    private String lahiVsKaug;
    private String tegelane;

    public Pihtasaamine() {

    }



    public String getLahiVsKaug() {
        return lahiVsKaug;
    }

    public void setLahiVsKaug(String lahiVsKaug) {
        this.lahiVsKaug = lahiVsKaug;
    }

    public String getTegelane() {
        return tegelane;
    }

    public void setTegelane(String tegelane) {
        this.tegelane = tegelane;
    }
}
