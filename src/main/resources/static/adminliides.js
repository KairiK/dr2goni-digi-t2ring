console.log("Tere, adminliidese konsool läks tööle.")

var kysiKasutajad = async function() {
	console.log("kysiKasutajad läks käima")
	var APIurl = url + '/kysikasutajad'
	var request = await fetch(APIurl)
	var kasutajad = await request.json()
    console.log(kasutajad)
	// kuva serverist saadud info html-is (ehk lehel)
	// document.querySelector('#muudaKasutajakontotValue').innerHTML = ""
	while (kasutajad.length > 0) {
		var kasutaja = kasutajad.pop()
		document.querySelector('#muudaKasutajakontotValue').innerHTML +=
		     "<option value='" + kasutaja + "'>" + kasutaja + "</option>"
	}
}

function kasutajakontosubmit() {
    console.log(document.querySelector('#muudaKasutajakontotValue').value)
    var kasutaja = document.querySelector('#muudaKasutajakontotValue').value
    localStorage.setItem('muudetavKasutaja', kasutaja);
    window.location.replace("/kasutaja_muutmine.html");
}

var kysiTegelased = async function() {
	console.log("kysiTegelased läks käima")
	var APIurl = url + '/kysitegelased'
	var request = await fetch(APIurl)
	var tegelased = await request.json()
    console.log(tegelased)
	// kuva serverist saadud info html-is (ehk lehel)
	// document.querySelector('#muudaTegelastValue').innerHTML = ""
	while (tegelased.length > 0) {
		var tegelane = tegelased.pop()
		document.querySelector('#muudaTegelastValue').innerHTML +=
		     "<option value='" + tegelane + "'>" + tegelane + "</option>"
	}
}

function tegelanesubmit() {
    console.log(document.querySelector('#muudaTegelastValue').value)
    var tegelane = document.querySelector('#muudaTegelastValue').value
    localStorage.setItem('muudetavTegelane', tegelane);
    window.location.replace("/tegelase_muutmine.html");
}

if(localStorage.getItem('kasutaja')==="admin"){
    console.log("On admin")
    kysiKasutajad()
    kysiTegelased()
}else{
    console.log("Ei ole admin")
    window.location.replace("/index.html")
}