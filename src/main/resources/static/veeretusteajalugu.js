console.log("VeeretusteAjalugu konsool läks tööle.")

async function veeretusteajalugu(){
    var APIurl = url + '/veeretusteajalugu'
    var andmed = await fetch(APIurl)
    var veeretused = await andmed.json()
    console.log(veeretused)
    if (veeretused.length<10) {
        while(veeretused.length>0) {
                var veeretus = veeretused.pop()
                console.log(veeretus)
                const aegDate = new Date(veeretus.aeg)
                var tund = aegDate.getHours()
                var minut = (aegDate.getMinutes()<10?'0':'') + aegDate.getMinutes()
                var sekund = (aegDate.getSeconds()<10?'0':'') + aegDate.getSeconds()
                document.querySelector("#koht").innerHTML += JSON.stringify(aegDate).slice(1, 11) + " " + tund + ":" + minut + ":" + sekund +  " | " + veeretus.tegelane + " | " +  veeretus.veeretused + " | " +  veeretus.lahing +"<br>"
        }
    } else {
        for (i = veeretused.length - 10; i < veeretused.length; i++) {
            var veeretus = veeretused[i]
            console.log(veeretus)
            const aegDate = new Date(veeretus.aeg)
            var tund = aegDate.getHours()
            var minut = aegDate.getMinutes()
            var sekund = aegDate.getSeconds()
            document.querySelector("#koht").innerHTML += JSON.stringify(aegDate).slice(1, 11) + " " + tund + ":" + minut + ":" + sekund +  " | " + veeretus.tegelane + " | " +  veeretus.veeretused + " | " +  veeretus.lahing +"<br>"
        }
    }

}

veeretusteajalugu()

document.querySelector('form').onsubmit = async function(event){
	event.preventDefault()
	var APIurl = url + '/kustuta'
    	var response = await fetch(APIurl, {
    		method: "POST",
    		body: JSON.stringify({}),
    		headers: {
    			'Accept': 'application/json',
    			'Content-Type':'application/json'
    		}
    	})
    document.querySelector("#koht").innerHTML = ""
}