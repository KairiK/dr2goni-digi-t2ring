var kasutaja = localStorage.getItem('kasutaja');
console.log("tere")
console.log("localStoragesse on salvestanud: " + kasutaja)
var tegelane = ""
var vigastus = 0
/* Kasutaja tegelese atribuutide info küsimine veebiserverist ja saadud vastuse
   veebilehele kuvamine.
*/
var refreshTabel = async function(){
    console.log("refreshTabel läks käima.")
    var andmed = await fetch("/tegelase_loomine", {
        method: "POST",
        body: JSON.stringify({kasutaja}),
        headers: {
        	'Accept': 'application/json',
        	'Content-Type':'application/json'
        }
    })
    var isikuList = await andmed.json()
    console.log(isikuList)
    for(var isik of isikuList){
        console.log(isik)
        document.querySelector("#kasutaja").innerHTML = "Kasutaja: " + isik.kasutaja
        document.querySelector("#tegelane").innerHTML = "Tegelane: " + isik.tegelane
        document.querySelector("#kaklus").value = isik.kaklus
        document.querySelector("#laskmine").value = isik.laskmine
        document.querySelector("#refleks").value = isik.refleks
        document.querySelector("#sitkus").value = isik.sitkus
        document.querySelector("#vaist").value = isik.vaist
        document.querySelector("#volu").value = isik.volu
        nimi = isik.tegelane
        vigastus = isik.vigastus
    }

}
refreshTabel()


// Peale kasutajalt kinnituse saamist saadetakse info veebiserverisse salvestamiseks.
document.querySelector('form').onsubmit = async function(event){
	event.preventDefault()
	// korjame kokku formist info
	console.log("Kinnita käivitus")

	var kaklus = document.querySelector("#kaklus").value
	var laskmine = document.querySelector("#laskmine").value
	var refleks = document.querySelector("#refleks").value
	var sitkus = document.querySelector("#sitkus").value
	var vaist = document.querySelector("#vaist").value
	var volu = document.querySelector("#volu").value

    console.log("haaras HTML-ist atribuudid")


	var APIurl = url + '/tegelase_kinnitus'
	var response = await fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({nimi, kaklus, laskmine, refleks, sitkus, vaist, volu, vigastus}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'
		}
	})
	window.localStorage.clear();
    localStorage.setItem('tegelane', nimi);
    console.log(response)
    window.location.replace("/tegevuse_valimine.html");
}






