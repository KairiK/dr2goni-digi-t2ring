console.log("tegevuse_valimine konsool käivitus")
var tegelane = localStorage.getItem('tegelane')

console.log("localStorage on salvestanud " + tegelane)

// Vigastuse kuvamine vormis
var refreshTabel = async function(){
    console.log("refreshTabel läks käima!")
    var andmed = await fetch("/mis_vigastused", {
        method: "POST",
        body: JSON.stringify({tegelane}),
        headers: {
        	'Accept': 'application/json',
        	'Content-Type':'application/json'
        }
    })
    var isikuList = await andmed.json()
    console.log(isikuList)
    for (var isik of isikuList) {
        console.log(isik)
        document.querySelector("#vigastus").value = isik.vigastus
    }

}
refreshTabel()



// Tegevuse valimine ja saatmine
document.querySelector('form').onsubmit = function(event){
	event.preventDefault()
	// korjame kokku formist info
	console.log("submit käivitus")

    /*toggle osa*/
    var rynnakVsKaitseInput=document.getElementById("togBtnRvsK").checked;
    console.log(rynnakVsKaitseInput)
    var rynnakVsKaitse = "";
    if (rynnakVsKaitseInput) {
        rynnakVsKaitse = "rynnak";
    } else {
        rynnakVsKaitse = "kaitse";
    }

    var lahiVsKaugInput=document.getElementById("togBtnLvsK").checked;
    console.log(lahiVsKaugInput)
    var lahiVsKaug = ""
    if (lahiVsKaugInput) {
        lahiVsKaug = "lahi"
    } else {
        lahiVsKaug = "kaug"
    }


	var vigastus = parseInt(document.querySelector("#vigastus").value)

    console.log(rynnakVsKaitse, lahiVsKaug, vigastus, tegelane)

	// POST päring postitab uue andmetüki serverisse
	var APIurl = url + '/vooru_alustamine'
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({rynnakVsKaitse: rynnakVsKaitse, lahiVsKaug: lahiVsKaug, vigastus: vigastus, tegelane: tegelane}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'
		}
	})

	localStorage.setItem('lahiVsKaug', lahiVsKaug);
	console.log(localStorage.getItem('lahiVsKaug'))

    if (rynnakVsKaitse == "rynnak") {
        window.location.replace("/pihtasaamine.html");
    } else {
        window.location.replace("/paasemine.html");
    }
}



