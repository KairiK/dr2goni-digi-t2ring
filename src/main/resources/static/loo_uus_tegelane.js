// Olemasolevate kasutajate  baasist küsimine ja drop-down menüüsse kuvamine
var kysiKasutajad = async function() {
	console.log("kysiKasutajad läks käima")
	var APIurl = url + '/kysikasutajad'
	var request = await fetch(APIurl)
	var kasutajad = await request.json()
    console.log(kasutajad)
    var index = kasutajad.indexOf("admin");
    if (index > -1) {
      kasutajad.splice(index, 1);
    }
	while (kasutajad.length > 0) {
		var kasutaja = kasutajad.pop()
		document.querySelector('#valiKasutajakontoValue').innerHTML +=
		     "<option value='" + kasutaja + "'>" + kasutaja + "</option>"
	}
}

kysiKasutajad()


// Tegelase info kokku kogumine ja serverisse saatmine
document.querySelector('form').onsubmit = async function(event){
	event.preventDefault()
	// korjame kokku formist info
	var kasutaja = document.querySelector("#valiKasutajakontoValue").value
	var tegelane = document.querySelector("#tegelaseNimi").value
	var kaklus = document.querySelector("#kaklus").value
	var laskmine = document.querySelector("#laskmine").value
	var refleks = document.querySelector("#refleks").value
	var sitkus = document.querySelector("#sitkus").value
	var vaist = document.querySelector("#vaist").value
	var volu = document.querySelector("#volu").value
	var vigastus = document.querySelector("#vigastus").value
	var aktiivsus = true

    console.log("Haaras HTMList andmed.")

	// POST päring postitab uue andmetüki serverisse
	var APIurl = url + '/loo_uus_tegelane'
	var response = await fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({kasutaja: kasutaja, nimi: tegelane, kaklus: kaklus, laskmine: laskmine, refleks: refleks, sitkus: sitkus, vaist: vaist, volu: volu, vigastus: vigastus, aktiivne: aktiivsus}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'
		}
	})

    // POST päringust tulnud vastus pritsitakse HTML-i.
	var tekstiBody = await response.text()
	    console.log(tekstiBody)
        document.querySelector("#response").innerHTML = tekstiBody
}