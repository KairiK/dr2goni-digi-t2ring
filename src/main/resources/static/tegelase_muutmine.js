var tegelane = localStorage.getItem('muudetavTegelane');
console.log("tere")
console.log("localStoragesse on salvestanud: " + tegelane)

document.querySelector('#tegelane').innerHTML = tegelane

// Tegelase info kokku kogumine ja serverisse saatmine.
var refreshTabel = async function(){
    console.log("refreshTabel läks käima.")
    console.log(localStorage.getItem('muudetavTegelane'))
    var tegelane = localStorage.getItem('muudetavTegelane')
    var andmed = await fetch("/tegelase_seis", {
        method: "POST",
        body: JSON.stringify({tegelane}),
        headers: {
        	'Accept': 'application/json',
        	'Content-Type':'application/json'
        }
    })
    var tegelaseList = await andmed.json()
    console.log(tegelaseList)
    for(var tegelane of tegelaseList){
        console.log(tegelane)
        document.querySelector("#kaklus").value = tegelane.kaklus
        document.querySelector("#laskmine").value = tegelane.laskmine
        document.querySelector("#refleks").value = tegelane.refleks
        document.querySelector("#sitkus").value = tegelane.sitkus
        document.querySelector("#vaist").value = tegelane.vaist
        document.querySelector("#volu").value = tegelane.volu
        document.querySelector("#vigastus").value = tegelane.vigastus
        console.log(tegelane.aktiivne)
        if(tegelane.aktiivne==true){
            console.log("valiti true")
            document.querySelector("#muudaAktiivsust").value = "Aktiivne"
        }else{
            console.log("valiti false")
            document.querySelector("#muudaAktiivsust").value = "Mitteaktiivne"
        }
    }

}
refreshTabel()

document.querySelector('#andmeteMuutmine').onsubmit = async function(event){
	event.preventDefault()
	// korjame kokku formist info
	var kaklus = document.querySelector("#kaklus").value
	var laskmine = document.querySelector("#laskmine").value
	var refleks = document.querySelector("#refleks").value
	var sitkus = document.querySelector("#sitkus").value
	var vaist = document.querySelector("#vaist").value
	var volu = document.querySelector("#volu").value
	var vigastus = document.querySelector("#vigastus").value
	if(document.querySelector("#muudaAktiivsust").value=="Aktiivne"){
        var aktiivsus = true
	}else{
	    var aktiivsus = false
	}


    console.log("Haaras HTMList andmed.")

	// POST päring postitab uue andmetüki serverisse
	var APIurl = url + '/tegelase_muutmine'
	var response = await fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({nimi: tegelane, kaklus: kaklus, laskmine: laskmine, refleks: refleks, sitkus: sitkus, vaist: vaist, volu: volu, vigastus: vigastus, aktiivne: aktiivsus}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'
		}
	})
	document.querySelector("#response").innerHTML = "Muudetud!"
}


function myFunction() {
    return confirm("Oled kindel, et soovid tegelast kustutada?");
}

document.querySelector('#tegelaseKustutamine').onsubmit = async function(event){
	event.preventDefault()
    vastus = myFunction()
    console.log(vastus)
    if(vastus){
    	// POST päring postitab uue andmetüki serverisse
    	var APIurl = url + '/tegelase_kustutamine'
    	var response = await fetch(APIurl, {
    		method: "POST",
    		body: JSON.stringify({tegelane: tegelane}),
    		headers: {
    			'Accept': 'application/json',
    			'Content-Type':'application/json'
    		}
    	})
    	document.querySelector("#response").innerHTML = "Tegelane kustutatud!"
    }

}