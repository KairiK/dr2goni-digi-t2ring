var kasutaja = localStorage.getItem('muudetavKasutaja');
console.log("tere")
console.log("localStoragesse on salvestanud: " + kasutaja)

document.querySelector('#kasutajanimi').innerHTML = kasutaja

document.querySelector('#parooliMuutmine').onsubmit = async function(event){
	event.preventDefault()
	// korjame kokku formist info
	var parool = document.querySelector("#parool").value

    console.log("Haaras HTMList parooli.")

	// POST päring postitab uue andmetüki serverisse
	var APIurl = url + '/kasutaja_muutmine'
	var response = await fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({kasutaja: kasutaja, parool: parool}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'
		}
	})
	document.querySelector("#response").innerHTML = "Muudetud!"
}

function kysiKinnitust() {
    return confirm("Oled kindel, et soovid tegelast kustutada?");
}

document.querySelector('#kasutajaKustutamine').onsubmit = async function(event){
	event.preventDefault()

	event.preventDefault()
    vastus = kysiKinnitust()
    console.log(vastus)

    if(vastus){
    	// POST päring postitab uue andmetüki serverisse
	    var APIurl = url + '/kasutaja_kustutamine'
	    var response = await fetch(APIurl, {
		    method: "POST",
		    body: JSON.stringify({kasutaja: kasutaja}),
		    headers: {
		        'Accept': 'application/json',
			    'Content-Type':'application/json'
		    }
	    })
	    document.querySelector("#response").innerHTML = "Kasutaja kustutatud!"
	}
}