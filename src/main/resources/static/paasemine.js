console.log("tere, paasemine konsool")
var tegelane = localStorage.getItem('tegelane')


// POST päring postitab uue andmetüki serverisse
var refreshTabel = async function(){
    var APIurl = url + '/paasemine'
    var andmed = await fetch(APIurl, {
    	method: "POST",
    	body: JSON.stringify({tegelane: tegelane}),
	    headers: {
	    	'Accept': 'application/json',
	    	'Content-Type':'application/json'
    	}
    })
    var veeretuseTulemus = await andmed.json()
    console.log(veeretuseTulemus)
    document.querySelector("#taringuveeretusedTulemus").innerHTML = veeretuseTulemus.pop()
    for (var veeretus of veeretuseTulemus) {
        console.log(veeretus)
        document.querySelector("#taringuveeretused").innerHTML += veeretus + "<br>"
    }
}
refreshTabel()

document.querySelector('form').onsubmit = async function(event){
	event.preventDefault()
	console.log("Vajutasid uuesti")
	var response = await fetch("/uuesti")
	var veeretuseTulemus = await response.json()
    console.log(veeretuseTulemus)
    document.querySelector("#taringuveeretused").innerHTML = ""
    document.querySelector("#taringuveeretusedTulemus").innerHTML = veeretuseTulemus.pop()
    for (var veeretus of veeretuseTulemus) {
        console.log(veeretus)
        document.querySelector("#taringuveeretused").innerHTML += veeretus + "<br>"
    }
}