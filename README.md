Projekti nimi: Drägoni digi-täring

Projekti eesmärk: Luua veeretusesimulaator, mis võtab tegelase vajalikud 
atribuudid ja arvutab koos täringuveeretusega õige tulemuse automaatselt
välja. 

User storyd: 
- Veebilehe avamisel tuleb uus konto luua või sisse logida (kasutajanimi 
ja parool)
    - uue konto loomisel küsitakse 
    1)nime, parooli, ja 
    2)tegelase atribuute
    - sisse logimisel kuvatakse atribuudid, saab vajadusel muuta
    
- Täringu veeretamine
    - lahinguvõte valida (rünnak või kaitse)
    - lähivõitlus või kaugvõitlus
    - täringu veeretamine (rakendus teeb vajalikud kalkulatsioonid)
    - lõppnumbri väljastamine
        - korduvate veeretuste puhul (st 1 ja 6 puhul) väljastab ka selle
        ajaloo...kuidagi


Must have:
- Peab olema masterUser, kellel peab olema võimalus vajadusel kasutaja parooli 
ja tegelaste atribuute muuta. See tuleb if lausega ja eraldi täiustatud vormiga,
kus saab muuta tegelase atribuute (kõike peale nime ja kasutaja). Lisaks saab
muuta teiste kasutajakontode nime ja parooli ning lisada.


Nice to have:

- Ühel kasutajal saab olla mitu tegelast, aga aktiivne saab olla vaid üks 
(st teised on kas teadmata kadunud või surnud). Logi pidamise jaoks.
- tegelase loomisel piirangud
- relvade ja kaitsekilpide lisamine ja nende keset päeva vahetamine
- täringute viskamise ajalugu ühe mängu raames (ajatempel koos tulemusega)
- Kui veeretusel viik, siis läheb veeretus kordamisele. Ilma selleta tuleb
  käsitsi veeretada.

Tehtud:
- Toggle Password Visibility - https://www.w3schools.com/howto/howto_js_toggle_password.asp